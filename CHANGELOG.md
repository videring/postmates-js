# 0.0.0
PostmatesJS, based on [postmate@1.5.2](https://www.npmjs.com/package/postmate), adds the following:
- support iframe dom
- support communicating with at least one iframe
- support communicating with `window.open`

# 0.0.1
- get 方法增加data参数

# 0.0.2
- 针对container为window.open或iframe dom的方式，握手通信方式从子页面onload方式改为直接发起握手方式

# 0.0.3
- build/下各文件version同步
