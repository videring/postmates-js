/* eslint-disable no-console */
import {
  ChildAPI,
  log,
  maxHandshakeRequests,
  generateNewMessageId,
  messageType,
  ParentAPI,
  PostmatesJS,
  resolveOrigin,
  resolveValue,
  sanitize,
} from '../../src/postmates-js'

test('Message Type', () => expect(messageType).toBe('application/x-postmates-js-v1+json'))

test('messageId', () => expect(!isNaN(generateNewMessageId())).toBe(true))

test('generateNewMessageId adds 1', () => {
  const result = generateNewMessageId()
  expect(result).toBe(2)
})

test('postmates-js logs args', () => {
  PostmatesJS.debug = true
  console.log = jest.fn()
  log('a', 'b', 'c')
  expect(typeof log !== 'undefined')
  expect(typeof log).toBe('function')
  expect(console.log.mock.calls[0][0]).toBe('a')
  expect(console.log.mock.calls[0][1]).toBe('b')
  expect(console.log.mock.calls[0][2]).toBe('c')
})

test('Sanitize', () => {
  expect(typeof sanitize !== 'undefined')
})

// test API
// the tests below test the API generally
test('PostmatesJS class is ready to rock', () => {
  expect(typeof PostmatesJS !== 'undefined')
  expect(typeof PostmatesJS).toBe('function')
  expect(typeof PostmatesJS.Model !== 'undefined')
  expect(typeof PostmatesJS.Model).toBe('function')
  expect(typeof PostmatesJS.Promise !== 'undefined')
  expect(typeof PostmatesJS.Promise).toBe('function')
})

test('ChildAPI class is ready to rock', () => {
  expect(typeof ChildAPI !== 'undefined')
  expect(typeof ChildAPI).toBe('function')
  expect(typeof ChildAPI.emit !== 'undefined')
})

test('maxHandshakeRequests class is ready to rock', () => {
  expect(maxHandshakeRequests === 5)
})

test('messageId class is ready to rock', () => {
  expect(typeof messageId !== 'undefined')
})

test('message_type class is ready to rock', () => {
  expect(typeof message_type !== 'undefined')
})

test('ParentAPI class is ready to rock', () => {
  expect(typeof ParentAPI !== 'undefined')
  expect(typeof ParentAPI).toBe('function')
})

test('resolveOrigin class is ready to rock', () => {
  expect(typeof resolveOrigin !== 'undefined')
  const result = 'https://sometest.com'
  const a = resolveOrigin(result)
  expect(a).toEqual(result)
})

test('resolveValue class is ready to rock', () => {
  expect(typeof resolveValue !== 'undefined')
})

test('sanitize class is ready to rock', () => {
  expect(typeof sanitize !== 'undefined')
})

// test mocks
// the tests below test the interworkings of PostmatesJS methods
test('PostmatesJS mocking', () => {
  PostmatesJS.debug = true
  const test = new PostmatesJS({
    container: document.body,
    url: 'http://child.com/',
    model: { foo: 'bar' },
  })
  expect(test.debug === true)
  expect(typeof test).toBe('object')
})

test('ChildAPI mocking', () => {
  PostmatesJS.debug = true
  const info = {
    model: { foo: 'bar' },
    parent: document.body,
    parentOrigin: 'https://parent.com',
    child: document.body,
    source: window,
  }
  const childMock = new ChildAPI(info)
  expect(typeof childMock).toBe('object')
})

test('ParentAPI mocking', () => {
  PostmatesJS.debug = true
  const info = {
    model: { foo: 'bar' },
    parent: document.body,
    parentOrigin: 'https://parent.com',
    child: document.body,
    source: window,
  }
  const parentMock = new ParentAPI(info)
  expect(typeof parentMock).toBe('object')
})
